import NavigationPage from "../components/Nav"

const IndexPage = () => {
  return (
    <div>
      <NavigationPage />
      <h1>Inicio</h1>
    </div>
  )
}

export default IndexPage
