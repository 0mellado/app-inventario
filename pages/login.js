import styles from '../styles/login.module.css'
import {useState} from "react"
import {useRouter} from "next/router"
import NavigationPage from "../components/Nav"

const LoginPage = () => {

    const router = useRouter()

    const [credentials, setCredentials] = useState({
        user: '',
        pwd: ''
    })

    const sendUser = async data => {
        const response = await fetch('/api/auth/login', {
            method: 'POST',
            body: JSON.stringify(data)
        })

        return response.json()
    }

    const handleChange = e => {
        setCredentials({
            ...credentials,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = async e => {
        e.preventDefault()
        const response = await sendUser(credentials)
        alert(response.data)
        if (response.success)
            router.push('/dashboard')
    }

    return (
        <div>
            <NavigationPage />
            <div className={styles.container}>
                <div>
                    <h2>Iniciar sesión</h2>
                    <form className={styles.form} onSubmit={handleSubmit}>
                        <label htmlFor='user'>Nombre de usuario o email</label>
                        <input
                            name="user"
                            type="text"
                            placeholder="user o email"
                            onChange={handleChange}
                            required
                        />
                        <label htmlFor='pwd'>Contraseña</label>
                        <input
                            name="pwd"
                            type="password"
                            placeholder="password"
                            onChange={handleChange}
                            required
                        />
                        <button type='submit'>
                            Iniciar sesión
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LoginPage 
