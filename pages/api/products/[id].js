import Product from '../../../models/product'

const productHandler = async (req, res) => {
    const {query: {id}, method} = req
    switch (method) {
        case 'PUT':
            try {
                const updateProd = JSON.parse(req.body)
                const product = await Product.findByIdAndUpdate(id, updateProd, {
                    new: true,
                    runValidators: true,
                })
                if (!product) {
                    return res.status(400).json({success: false})
                }
                res.status(200).json({success: true, prod: product})
            } catch (error) {
                res.status(400).json({success: false})
            }
            break
        case 'DELETE':
            try {
                const deletedProd = await Product.deleteOne({_id: id})
                if (!deletedProd) {
                    return res.status(400).json({success: false})
                }
                res.status(200).json({success: true})
            } catch (error) {
                res.status(400).json({success: false})
            }
            break
    }
}

export default productHandler
