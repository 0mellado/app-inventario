import dbConnect from '../../../lib/db'
import User from '../../../models/user'
import bycrypt from 'bcrypt'
import {sign, verify} from "jsonwebtoken"
import {serialize} from "cookie"

const loginHandler = async (req, res) => {

    const {tokenName} = req.cookies

    try {
        verify(tokenName, process.env.SECRET)
        res.status(300).json({
            success: true,
            data: process.env.YOU_ARE_LOGIN
        })
    } catch (err) {
        const {method} = req
        if (method != 'POST')
            return res.status(403).json({
                success: false,
                data: process.env.FORBIDDEN
            })

        const userJson = JSON.parse(req.body)

        try {
            await dbConnect()

            const user = await User.findOne({
                $or: [
                    {username: userJson.user},
                    {email: userJson.user}
                ]
            })

            if (!user || !bycrypt.compareSync(userJson.pwd, user.pwd))
                return res.status(401).json({
                    success: false,
                    data: process.env.LOGIN_INVALID
                })

            const token = sign({
                exp: Math.floor(Date.now() / 1000) + 60 * 60 * 24 * 30,
                email: user.email,
                username: user.username,
                admin: user.admin
            }, process.env.SECRET)

            const serialized = serialize('tokenName', token, {
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production',
                sameSite: 'lax',
                maxAge: 1000 * 60 * 60 * 24 * 30,
                path: '/'
            })

            res.setHeader('Set-Cookie', serialized)
            res.status(201).json({
                success: true,
                data: process.env.LOGIN_SUCCESSFULLY
            })
        } catch (err) {
            res.status(400).json({
                success: false,
                data: process.env.MONGODB_CONNECT_ERROR
            })
        }
    }
}

export default loginHandler
