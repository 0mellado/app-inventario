import Product from '../../models/product'
import dbConnect from '../../lib/db'
import {verify} from "jsonwebtoken"

const profileHandler = async (req, res) => {

    const token = req.headers['x-access-token']

    if (!token)
        return res.status(401).json({
            success: false,
            data: process.env.YOU_ARENT_LOGIN
        })

    try {
        verify(token, process.env.SECRET)
        try {
            await dbConnect()
            const products = await Product.find()
            res.status(200).json(products)
        } catch (err) {
            console.error(err)
        }
    } catch (err) {
        return res.status(401).json({
            success: false,
            data: process.env.INVALID_TOKEN
        })
    }
}

export default profileHandler
