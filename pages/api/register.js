import bcrypt from 'bcrypt'
import dbConnect from '../../lib/db'
import User from '../../models/user'

const registerHandler = async (req, res) => {

    const {method} = req
    const userJson = JSON.parse(req.body)

    const newUser = {
        username: userJson.username,
        admin: false,
        email: userJson.email,
        pwd: bcrypt.hashSync(userJson.pwd, 10),
    }

    if (method != 'POST')
        return res.status(403).json({
            success: false,
            data: process.env.FORBIDDEN
        })

    try {
        await dbConnect()
        const userQuery = await User.findOne({
            "$or": [
                {username: userJson.username},
                {email: userJson.email}
            ]
        })

        if (userQuery)
            return res.status(418).json({
                success: false,
                data: process.env.USER_EXIST
            })

        const user = await User.create(newUser)

        if (!user)
            return res.status(400).json({
                success: true,
                data: process.env.USER_REGISTERED_ERROR
            })

        res.status(201).json({
            success: true,
            data: process.env.USER_REGISTERED
        })
    } catch (err) {
        res.status(400).json({
            success: false,
            data: process.env.MONGODB_CONNECT_ERROR
        })
    }
}

export default registerHandler
