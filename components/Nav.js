import Link from "next/link"
import styles from "../styles/Nav.module.css"

const links = [
    {
        label: 'Inicio',
        route: '/'
    },
    {
        label: 'Iniciar sesión',
        route: '/login'
    },
    {
        label: 'Registrarse',
        route: '/register'
    },
    {
        label: 'Inventario',
        route: '/dashboard'
    }
]

const nav = links.map(({label, route}) =>
    <li key={route}>
        <Link href={route}>{label}</Link>
    </li>
)

const NavigationPage = () => (
    <header className={styles.header}>
        <nav>
            <ul className={styles.navigation}>
                {nav}
            </ul>
        </nav>
    </header>
)

export default NavigationPage
