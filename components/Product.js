import styles from '../styles/products.module.css'

const ProductC = (prod) => {
    return (
        <div className={styles.tbuttons}>
            <div className={styles.tbody}>
                <p className={styles.item}>{prod.name}</p>
                <p className={styles.item}>{prod.stock}</p>
                <p className={styles.item}>$ {prod.price}</p>
                <p className={styles.item}>{prod.weight} kg</p>
                <p className={styles.item}>$ {prod.stock * prod.price}</p>
            </div>
            <button className={styles.updbtn}>Editar</button>
            <button className={styles.delbtn}>Eliminar</button>
        </div>
    )
}

export default ProductC
